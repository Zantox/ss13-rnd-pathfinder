import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.*;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

class Item {
    public String name;
    //0: Autolathe, 1: Protolathe,
    //2: Imprinter, 3: Mechfab,
    //4: Smelter, 5: Biogen
    public boolean[] buildType = new boolean[6];
    //0: Metal, 1: Glass, 2: Silver, 3: Gold,
    //4: Uranium, 5: Plasma, 6: Diamond,
    //7: Titanium, 8: Bluespace, 9: Bananium
    public int[] cost = new int[10];
    //0: Materials, 1: Engineering, 2: Plasma,
    //3: Power, 4: Bluespace, 5: Biotech,
    //6: Combat, 7: Magnets, 8: Data,
    //9: Illegal
    public int[] reqTech = new int[10];
    public int[] originTech = new int[10];
    public boolean locked;

    static public Item[] filter(Item[] array, boolean[] machine, boolean[] cost, boolean locked, boolean worlditems) {
        List<Item> output = new ArrayList<>();
        String[] worlditem = {"Welding Tool", "Screwdriver", "Wrench",
                              "Pocket Crowbar", "Analyzer", "Wirecutters"};
        for (int i = 0; i < array.length; i++) {
            boolean suitable = false;
            for (int o = 0; o < machine.length; o++) {
                if (machine[o] && array[i].buildType[o]) {
                    suitable = true;
                    break;
                }
            }
            for (int o = 0; o < cost.length; o++) {
                if (!cost[o] && array[i].cost[o] != 0) {
                    suitable = false;
                    break;
                }
            }
            if (!locked && array[i].locked) {
                suitable = false;
            }
            if (worlditems) {
                for (int o = 0; o < worlditem.length; o++) {
                    if (array[i].name.equals(worlditem[o])) {
                        suitable = true;
                        break;
                    }
                }
            }

            if (suitable) {
                output.add(array[i]);
            }
        }
        return output.toArray(new Item[0]);
    }

    public int totalCost() {
        int total = 0;
        for (int i = 0; i < cost.length; i++)
            total += cost[i];
        return total;
    }
}

class FileParser {
    public static String[][] filterFile(String filename, String pattern) throws FileNotFoundException {
        Pattern filter = Pattern.compile(pattern);
        List<List<String>> matches = new ArrayList<List<String>>();

        Scanner scanner = new Scanner(new File(filename));
        Matcher matcher;
        while (scanner.hasNextLine()) {
            String line = scanner.nextLine();
            matcher = filter.matcher(line);
            if (matcher.find()) {
                matches.add(new ArrayList<String>());
                for (int i = 0; i <= matcher.groupCount(); i++) {
                    matches.get(matches.size() - 1).add(matcher.group(i));
                }
            }
        }
        scanner.close();

        String[][] output = new String[matches.size()][];
        for (int i = 0; i < output.length; i++) {
            output[i] = matches.get(i).toArray(new String[0]);
        }
        return output;
    }

    public static String[][] createInitialFile(String path, String resultname) throws FileNotFoundException {
        final String DESIGNSFOLDER = path + "modules/research/designs/";

        File file = new File(DESIGNSFOLDER);
        String[] filesInFolder = file.list();
        PrintWriter result = new PrintWriter(resultname);
        String pattern = "(?:^/datum.+)|(?:\\s(?:name|req_tech|build_type|materials|build_path|locked)\\s=.+)|(?:^$)";
        for (int i = 0; i < filesInFolder.length; i++) {
            String[][] lines = FileParser.filterFile(DESIGNSFOLDER + filesInFolder[i], pattern);
            for (int o = 0; o < lines.length; o++) {
                if (i != 0 && o != 0) {
                    if (!(lines[o][0].equals("") && lines[o - 1][0].equals("")))
                        result.println(lines[o][0]);
                }

            }
        }
        result.close();
        pattern = "\\sbuild_path\\s=\\s(.+)";
        String[][] itemNames = FileParser.filterFile(resultname, pattern);
        for (int i = 0; i < itemNames.length; i++) {
            itemNames[i][0] = itemNames[i][1];
            itemNames[i][1] = null;
        }

        return itemNames;
    }

    public static void findOriginTech(String filename, String[][] originTech) throws FileNotFoundException {
        Scanner scanner = new Scanner(new File(filename));
        Matcher matcher;
        String paragraphFirstLine = "";
        String paragraph = "";
        Pattern paragraphStart = Pattern.compile("^(?:\\w|/)\\w");
        Pattern techPattern = Pattern.compile("\\sorigin_tech\\s=\\s\".+\"");
        while (scanner.hasNextLine()) {
            String line = scanner.nextLine();
            matcher = paragraphStart.matcher(line);
            if (matcher.find() && !paragraph.equals("") || !scanner.hasNextLine()) {
                //A new paragraph is found, or last paragraph of file,
                //process the last paragraph.
                for (int i = 0; i < originTech.length; i++) {
                    if (paragraphFirstLine.equals(originTech[i][0])) {
                        matcher = techPattern.matcher(paragraph);
                        if (matcher.find()) {
                            originTech[i][1] = matcher.group(0);
                        }
                        break;
                    }
                }
                paragraphFirstLine = line;
                paragraph = "";
            }
            paragraph += line + '\n';
        }
        scanner.close();
    }

    public static void parseFolder(String pathname, String[][] originTech) throws FileNotFoundException {
        File file = new File(pathname);
        String[] fileNames = file.list();
        for (int i = 0; i < fileNames.length; i++) {
            String filePath = pathname + '/' + fileNames[i];
            file = new File(filePath);
            if (file.isDirectory()) {
                parseFolder(filePath, originTech);
            } else {
                findOriginTech(filePath, originTech);
            }
        }
    }

    public static void insertOriginTech(String pathname, String[][] originTech) throws FileNotFoundException {
        Scanner scanner = new Scanner(new File(pathname));
        String tempFileName = "tempData.tmp";
        PrintWriter newFile = new PrintWriter(tempFileName);
        Pattern pattern = Pattern.compile("\\sbuild_path\\s=\\s(.+)");
        Matcher matcher;
        while (scanner.hasNextLine()) {
            String line = scanner.nextLine();
            newFile.println(line);
            matcher = pattern.matcher(line);
            if (matcher.find()) {
                String build_path = matcher.group(1);
                for (int i = 0; i < originTech.length; i++) {
                    if (build_path.equals(originTech[i][0])) {
                        if (originTech[i][1] != null)
                            newFile.println(originTech[i][1]);
                        break;
                    }
                }
            }
        }
        scanner.close();
        newFile.close();
        File file = new File(pathname);
        file.delete();
        File tempFile = new File(tempFileName);
        tempFile.renameTo(file);
    }

    public static Item[] generateArray(String pathname) throws FileNotFoundException {
        Scanner scanner = new Scanner(new File(pathname));
        List<Item> list = new ArrayList<Item>();
        String paragraph = "";
        Pattern name = Pattern.compile("\\sname\\s=\\s\"(.+)\"");
        Pattern buildType = Pattern.compile("\\sbuild_type\\s=\\s([A-Za-z |]+)");
        Pattern cost = Pattern.compile("\\smaterials\\s=\\slist\\((.+)\\)");
        Pattern reqTech = Pattern.compile("\\sreq_tech\\s=\\slist\\((.+)\\)");
        Pattern originTech = Pattern.compile("\\sorigin_tech\\s=\\s\"(.+)\"");
        Pattern locked = Pattern.compile("\\slocked\\s=\\s1");
        Matcher matcher;
        while (scanner.hasNextLine()) {
            String line = scanner.nextLine();
            paragraph += line + "\n";
            if (line.equals("")) {
                boolean suitable = true;
                matcher = buildType.matcher(paragraph);
                if (!matcher.find())
                    suitable = false;

                if (suitable) {
                    list.add(new Item());
                    int index = list.size() - 1;
                    matcher = name.matcher(paragraph);
                    matcher.find();
                    list.get(index).name = matcher.group(1).replace("'", "\\'");

                    matcher = buildType.matcher(paragraph);
                    matcher.find();
                    String result = matcher.group(1);
                    Pattern pattern = Pattern.compile("\\w+");
                    matcher = pattern.matcher(result);
                    while (matcher.find()) {
                        switch (matcher.group(0)) {
                            case "AUTOLATHE":
                                list.get(index).buildType[0] = true;
                                break;
                            case "PROTOLATHE":
                                list.get(index).buildType[1] = true;
                                break;
                            case "IMPRINTER":
                                list.get(index).buildType[2] = true;
                                break;
                            case "MECHFAB":
                                list.get(index).buildType[3] = true;
                                break;
                            case "BIOGENERATOR":
                                list.get(index).buildType[4] = true;
                                break;
                            default:
                                break;
                        }
                    }

                    matcher = cost.matcher(paragraph);
                    matcher.find();
                    result = matcher.group(1);
                    pattern = Pattern.compile("MAT_(\\w+)\\s?=\\s?(\\d+)");
                    matcher = pattern.matcher(result);
                    while (matcher.find()) {
                        switch (matcher.group(1)) {
                            case "METAL":
                                list.get(index).cost[0] = Integer.parseInt(matcher.group(2));
                                break;
                            case "GLASS":
                                list.get(index).cost[1] = Integer.parseInt(matcher.group(2));
                                break;
                            case "SILVER":
                                list.get(index).cost[2] = Integer.parseInt(matcher.group(2));
                                break;
                            case "GOLD":
                                list.get(index).cost[3] = Integer.parseInt(matcher.group(2));
                                break;
                            case "URANIUM":
                                list.get(index).cost[4] = Integer.parseInt(matcher.group(2));
                                break;
                            case "PLASMA":
                                list.get(index).cost[5] = Integer.parseInt(matcher.group(2));
                                break;
                            case "DIAMOND":
                                list.get(index).cost[6] = Integer.parseInt(matcher.group(2));
                                break;
                            case "TITANIUM":
                                list.get(index).cost[7] = Integer.parseInt(matcher.group(2));
                                break;
                            case "BLUESPACE":
                                list.get(index).cost[8] = Integer.parseInt(matcher.group(2));
                                break;
                            case "BANANIUM":
                                list.get(index).cost[9] = Integer.parseInt(matcher.group(2));
                                break;
                            default:
                                break;
                        }
                    }


                    matcher = reqTech.matcher(paragraph);
                    if (matcher.find()) {
                        result = matcher.group(1);
                        pattern = Pattern.compile("\"(\\w+)\"\\s=\\s(\\d)");
                        matcher = pattern.matcher(result);
                        while (matcher.find()) {
                            switch (matcher.group(1)) {
                                case "materials":
                                    list.get(index).reqTech[0] = Integer.parseInt(matcher.group(2));
                                    break;
                                case "engineering":
                                    list.get(index).reqTech[1] = Integer.parseInt(matcher.group(2));
                                    break;
                                case "plasmatech":
                                    list.get(index).reqTech[2] = Integer.parseInt(matcher.group(2));
                                    break;
                                case "powerstorage":
                                    list.get(index).reqTech[3] = Integer.parseInt(matcher.group(2));
                                    break;
                                case "bluespace":
                                    list.get(index).reqTech[4] = Integer.parseInt(matcher.group(2));
                                    break;
                                case "biotech":
                                    list.get(index).reqTech[5] = Integer.parseInt(matcher.group(2));
                                    break;
                                case "combat":
                                    list.get(index).reqTech[6] = Integer.parseInt(matcher.group(2));
                                    break;
                                case "magnets":
                                    list.get(index).reqTech[7] = Integer.parseInt(matcher.group(2));
                                    break;
                                case "programming":
                                    list.get(index).reqTech[8] = Integer.parseInt(matcher.group(2));
                                    break;
                                case "syndicate":
                                    list.get(index).reqTech[9] = Integer.parseInt(matcher.group(2));
                                    break;
                            }
                        }
                    }

                    matcher = originTech.matcher(paragraph);
                    if (matcher.find()) {
                        result = matcher.group(1);
                        pattern = Pattern.compile("(\\w+)=(\\d)");
                        matcher = pattern.matcher(result);
                        while (matcher.find()) {
                            switch (matcher.group(1)) {
                                case "materials":
                                    list.get(index).originTech[0] = Integer.parseInt(matcher.group(2));
                                    break;
                                case "engineering":
                                    list.get(index).originTech[1] = Integer.parseInt(matcher.group(2));
                                    break;
                                case "plasmatech":
                                    list.get(index).originTech[2] = Integer.parseInt(matcher.group(2));
                                    break;
                                case "powerstorage":
                                    list.get(index).originTech[3] = Integer.parseInt(matcher.group(2));
                                    break;
                                case "bluespace":
                                    list.get(index).originTech[4] = Integer.parseInt(matcher.group(2));
                                    break;
                                case "biotech":
                                    list.get(index).originTech[5] = Integer.parseInt(matcher.group(2));
                                    break;
                                case "combat":
                                    list.get(index).originTech[6] = Integer.parseInt(matcher.group(2));
                                    break;
                                case "magnets":
                                    list.get(index).originTech[7] = Integer.parseInt(matcher.group(2));
                                    break;
                                case "programming":
                                    list.get(index).originTech[8] = Integer.parseInt(matcher.group(2));
                                    break;
                                case "syndicate":
                                    list.get(index).originTech[9] = Integer.parseInt(matcher.group(2));
                                    break;
                            }
                        }
                    }

                    matcher = locked.matcher(paragraph);
                    if (matcher.find())
                        list.get(index).locked = true;
                }

                paragraph = "";
            }
        }
        scanner.close();
        return list.toArray(new Item[0]);
    }

    public static void produceJSfile(String filename, Item[] items) throws FileNotFoundException {
        String[] machines = {"AUTOLATHE", "PROTOLATHE", "IMPRINTER", "MECHFAB"};
        String[] tech = {"m", "e", "pl", "pow", "bs", "bio", "c", "em", "dt", "i"};
        PrintWriter output = new PrintWriter(new File(filename));
        output.print("rndData = [");
        for (int i = 0; i < items.length; i++) {
            output.print("{'name':'" + items[i].name + "',");
            output.print("'buildType':'");
            boolean first = true;
            for (int o = 0; o < machines.length; o++) {
                if (items[i].buildType[o]) {
                    if (!first)
                        output.print(" | ");
                    output.print(machines[o]);
                    first = false;
                }
            }
            output.print("','numCost':");
            output.print(items[i].totalCost() + ",'reqTech':{");
            first = true;
            for (int o = 0; o < tech.length; o++) {
                if (!first)
                    output.print(',');
                output.print("'" + tech[o] + "':" + items[i].reqTech[o]);
                first = false;
            }
            output.print("},'originTech':{");
            first = true;
            for (int o = 0; o < tech.length; o++) {
                if (!first)
                    output.print(',');
                output.print("'" + tech[o] + "':" + items[i].originTech[o]);
                first = false;
            }
            output.print("}}," + '\n');
        }
        output.print("];");
        output.close();
    }
}

class Node {
    public int[] stats;
    public String itemName;
    public Node previous;
    public int depth;

    Node(int[] stats, int depth) {
        this.stats = stats;
        this.depth = depth;
    }
    Node (int[] stats, String itemName, Node previous) {
        this(stats, previous.depth + 1);
        this.itemName = itemName;
        this.previous = previous;
    }
}

class PathFinder {
    private static int[] goalStats = {5,5,5,5,5,6,5,5,6,1};
    private static Node shortest = new Node(goalStats, Integer.MAX_VALUE);

    public static String[] findPath(Item[] items) {
        Node origin = new Node(new int[]{1,1,1,1,1,1,1,1,1,1}, 0);
        depthFirst(items, origin);
        List<String> path = new ArrayList<>();
        //System.out.println(shortest.itemName);
        return path.toArray(new String[0]);
    }

    private static void depthFirst(Item[] items, Node node) {
        //if (node.depth == 14)
        //    System.out.println(node.itemName);

        //This checks if the current node being evaluated has
        //a chance to become the next shortest or not.
        int depthDiff = shortest.depth - node.depth;
        for (int i = 0; i < goalStats.length; i++)
            if (depthDiff <= goalStats[i] - node.stats[i])
                return;


        //This checks if the node being evaluated has reached
        //maximum stats.
        if (Arrays.equals(node.stats, goalStats)) {
            if (node.depth < shortest.depth) {
                shortest = node;
                System.out.println(shortest.depth);
                Node current = shortest;
                while (current.itemName != null) {
                    System.out.println(current.itemName);
                    current = current.previous;
                }
                return;
            }
        }

        //The 7 is just a constant I chose after trying
        //a couple and picking the one that performed the
        //fastest.
        //This function cleans the items array up and
        //leaves only items which have originTech levels
        //high enough to raise tech levels.
        if (node.depth < shortest.depth - 7) {
            List<Item> tempList = new ArrayList<>();
            for (Item item : items)
                if (higherOrigin(node.stats, item))
                    tempList.add(item);
            items = tempList.toArray(new Item[0]);
        }

        //The 2 is just a constant I chose after trying
        //a couple and picking the one that performed the
        //fastest.
        //This function filters out items so that it will
        //only evaluate items that raise the tech level as
        //much as possible. For example if one thing raises
        //materials, but another item would increase materials
        //and data, it will only evaluate the item that would
        //increase two.
        if (node.depth < shortest.depth - 2) {
            List<Item> toEvaluate = new ArrayList<>();
            for (int i = 0; i < items.length; i++) {
                if (canMake(node.stats, items[i])) {
                    boolean alreadyIn = false;
                    for (int o = 0; o < toEvaluate.size(); o++) {
                        if (sameButHigher(toEvaluate.get(o), items[i])) {
                            toEvaluate.set(o, items[i]);
                            break;
                        } else if (sameButHigher(items[i], toEvaluate.get(o)) ||
                                Arrays.equals(toEvaluate.get(o).originTech, items[i].originTech)) {
                            alreadyIn = true;
                            break;
                        }
                    }
                    if (!alreadyIn)
                        toEvaluate.add(items[i]);
                }
            }

            for (Item item : toEvaluate) {
                Node next = new Node(risesLevels(node.stats, item), item.name, node);
                depthFirst(items, next);
            }
        }
        else {
            for (Item item : items) {
                if (canMake(node.stats, item)) {
                    Node next = new Node(risesLevels(node.stats, item), item.name, node);
                    depthFirst(items, next);
                }
            }
        }

    }

    static private boolean sameButHigher(Item i1, Item i2) {
        //Checks if i2 has same but higher stats than i1.
        boolean higher = false;
        for (int i = 0; i < i1.originTech.length; i++) {
            if (i1.originTech[i] > i2.originTech[i])
                return false;
            if (i2.originTech[i] > i1.originTech[i])
                higher = true;
        }
        return higher;
    }

    static private boolean higherOrigin(int[] stats, Item item) {
        for (int i = 0; i < stats.length; i++)
            if (item.originTech[i] >= stats[i])
                return true;
        return false;
    }

    static private boolean canMake(int[] stats, Item item) {
        //Checks if you can make an item and if destructing
        //that item would increase your stats at all.
        for (int i = 0; i < stats.length; i++) {
            if (stats[i] < item.reqTech[i])
                return false;
        }
        for (int i = 0; i < stats.length; i++) {
            if (stats[i] <= item.originTech[i])
                return true;
        }
        return false;
    }

    static private int[] risesLevels(int[] stats, Item item) {
        int[] output = new int[stats.length];
        for (int i = 0; i < stats.length; i++) {
            if (stats[i] <= item.originTech[i]) {
                output[i] = stats[i] + 1;
            }
            else {
                output[i] = stats[i];
            }
        }
        return output;
    }
}

public class Main {
    public static void main(String[] args) throws IOException {
        final String FOLDERNAME = "code/";
        final String RESULTNAME = "data.txt";
        final String JSFILENAME1 = "rndData.js";
        final String JSFILENAME2 = "rndDataOnlyGlassAndMetal.js";
        byte options = 0x00;
        if (args.length > 0) {
            for (int i = 1; i < args[0].length(); i++) {
                switch (args[0].charAt(i)) {
                    case 'd': //Generate data.txt file
                        options |= 0x01;
                        break;
                    case 'j': //Produce JS file.
                        options |= 0x02;
                        break;
                    default:
                        break;
                }
            }

        }

        if (!new File(RESULTNAME).exists())
            options = (byte)(options | 1);
        if ((options & 1) == 1) {
            String[][] items = FileParser.createInitialFile(FOLDERNAME, RESULTNAME);
            FileParser.parseFolder(FOLDERNAME, items);
            FileParser.insertOriginTech(RESULTNAME, items);
        }

        Item[] itemArray = FileParser.generateArray(RESULTNAME);
        Arrays.sort(itemArray, new Comparator<Item>() {
                    @Override
                    public int compare(Item o1, Item o2) {
                        return o1.name.compareTo(o2.name);
                    }
                });

        if ((options >> 1 & 1) == 1) {
            FileParser.produceJSfile(JSFILENAME1, itemArray);
            boolean[] allowedMachines = {false, true, true, false, false, false};
            boolean[] allowedMaterials = {true, true, false, false, false,
                    false, false, false, false, false};
            itemArray = Item.filter(itemArray, allowedMachines, allowedMaterials, false, true);
            FileParser.produceJSfile(JSFILENAME2, itemArray);
        }

        boolean[] allowedMachines = {false, true, true, false, false, false};
        boolean[] allowedMaterials = {true, true, false, false, false,
                false, false, false, false, false};
        itemArray = Item.filter(itemArray, allowedMachines, allowedMaterials, false, true);
        Arrays.sort(itemArray, new Comparator<Item>() {
            @Override
            public int compare(Item o1, Item o2) {

                return o1.totalCost() - o2.totalCost();
            }
        });

        String[] path = PathFinder.findPath(itemArray);
        for (String node : path)
            System.out.println(node);
    }
}
